import { combineReducers } from "redux";
import reducer from "@reduxs/fetchdata/fetchdata.reducer";
const rootReducer = combineReducers({
  data: reducer,
});

export type RootState = ReturnType<typeof rootReducer>;
export default rootReducer;
