import { FETCH_DATA } from "./types";

interface Data {
  type: String;
  payload: [];
}

const initilaState = {
  foo: [],
};

const reducer = (state = initilaState, action: Data) => {
  switch (action.type) {
    case FETCH_DATA:
      return { ...state, foo: action.payload };
    default:
      return { ...state };
  }
};

export default reducer;
