import React from "react";
import Link from "next/link";
import { useRouter } from "next/router";
const navitem = [
  { name: "Home", link: "/", active: "/[supplier]" },
  { name: "About Us", link: "/aboutus", active: "/[supplier]/aboutus" },
  { name: "Solutions", link: "/solutions", active: "/[supplier]/sloutions" },
  {
    name: "Our Gallery",
    link: "/ourgallery",
    active: "/[supplier]/ourgallery",
  },
  { name: "Contact Us", link: "/contactus", active: "/[supplier]/contactus" },
];
const SearchNavItem: React.FC = () => {
  const router = useRouter();

  const { supplier } = router.query;
  return (
    <div className="flex items-baseline space-x-0  ">
      {navitem.map((item, i) => (
        <Link key={i} href={`/${supplier}${item.link}`}>
          <a
            key={i}
            className={`${
              router.pathname == item.active
                ? "bg-second_color text-primary"
                : "text-white"
            }   text-lg font-bold leading-[54px] cursor-pointer hover:bg-second_color hover:text-primary color  block h-full px-6 `}
          >
            {item.name}
          </a>
        </Link>
      ))}
    </div>
  );
};

export default SearchNavItem;
