import React from "react";
import PhotoCard from "./PhotoCard.component";
const Product = [
  {
    id: 0,
    name: "hello producr",
    price: 4000,
    image: "/images/product/mobile-1.jpg",
  },
  {
    id: 1,
    name: "HP ZBook Fury G8 (17.3-inch)",
    image: "/images/laptop/key1.jpg",
    price: 40000,
  },
  {
    id: 2,
    name: "HP ZBook Fury G8 (15.6-inch)",
    image: "/images/laptop/key2.jpg",
    price: 40000,
  },
  {
    id: 3,
    name: "Apple MacBook Air (M1 2020) Apple MacBook Air (M1 2020)Apple MacBook Air (M1 2020)",
    image: "/images/laptop/key3.jpg",
    price: 40000,
  },
  {
    id: 4,
    name: "Microsoft Surface Laptop 4",
    image: "/images/laptop/key-4.jpg",
    price: 40000,
  },
  {
    id: 5,
    name: "Dell XPS 15 (2020)",
    image: "/images/laptop/l1.jpg",
    price: 40000,
  },
  {
    id: 6,
    name: "HP Spectre x360 (2021)",
    image: "/images/laptop/l2.png",
    price: 40000,
  },
  {
    id: 7,
    name: "Dell XPS 13 (Late 2020)",
    image: "/images/laptop/l3.png",
    price: 40000,
  },
  {
    id: 8,
    name: "LG Gram 17 (2021)",
    image: "/images/laptop/l4.jpg",
    price: 40000,
  },
  { id: 9, name: "Acer Swift 3", image: "/images/laptop/l5.jpg", price: 40000 },
  {
    id: 10,
    name: " MacBook Pro (16-inch, 2019)",
    image: "/images/laptop/l6.jpg",
    price: 40000,
  },
  {
    id: 11,
    name: "Asus ROG Zephyrus G14",
    image: "/images/laptop/l7.jpg",
    price: 40000,
  },
  {
    id: 12,
    name: "Asus TUF Dash F15",
    image: "/images/laptop/l8.jpg",
    price: 40000,
  },
  {
    id: 13,
    name: "HP Elite Dragonfly",
    image: "/images/laptop/l9.jpg",
    price: 40000,
  },
  {
    id: 14,
    name: "HP Envy x360 13 (2020)",
    image: "/images/laptop/l10.jpg",
    price: 40000,
  },
  {
    id: 15,
    name: "Google Pixelbook Go",
    image: "/images/laptop/l11.png",
    price: 40000,
  },
  {
    id: 16,
    name: "Lenovo IdeaPad Duet Chromebook",
    image: "/images/laptop/l12.png",
    price: 40000,
  },
  {
    id: 17,
    name: "phont motorala x967 hadn set",
    image: "/images/product/phone-2.jpg",
    price: 40000,
  },
  {
    id: 18,
    name: "Samsung galaxy mobile n975",
    image: "/images/product/phone-3.jpg",
    price: 40000,
  },
  {
    id: 19,
    name: "Samsung galaxy powerbake potable n975",
    image: "/images/product/pb-2.jpg",
    price: 40000,
  },
];

const Gallery: React.FC = () => {
  return (
    <div className="grid grid-cols-6 ">
      {Product.map((item, i) => (
        <PhotoCard key={i} index={i} />
      ))}
    </div>
  );
};

export default Gallery;
