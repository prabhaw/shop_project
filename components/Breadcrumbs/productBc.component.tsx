import React from "react";
import Link from "next/link";

import Breadcrumbs from "@components/Breadcrumbs/Breadcrumbs.component";
const ProductBC: React.FC = () => {
  return (
    <div className="py-4">
      <Breadcrumbs>
        <li>
          <Link href="/">
            <a className="text-blue font-bold">Home</a>
          </Link>
        </li>
        <li>
          <span className="mx-2">/</span>
        </li>
        <li>
          <Link href="/a2zmobilehouse">
            <a className="text-blue font-bold">a2z mobile house</a>
          </Link>
        </li>
        <li>
          <span className="mx-2">/</span>
        </li>
        <li>
          <Link href="/mobilephone">
            <a className="text-blue font-bold">mobile phone</a>
          </Link>
        </li>
        <li>
          <span className="mx-2">/</span>
        </li>
        <li>
          <Link href="/">
            <a className="text-blue font-bold line-clamp-1 w-[300px]">
              samsung glaxy m21 samsung glaxy m21 samsung glaxy m21 samsung
              glaxy m21 samsung glaxy m21 samsung glaxy m21 samsung glaxy m21
            </a>
          </Link>
        </li>
      </Breadcrumbs>
    </div>
  );
};

export default ProductBC;
