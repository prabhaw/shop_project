import React, { ReactChildren, ReactChild, useEffect, useState } from "react";

interface BradData {
  children: ReactChild | ReactChild[] | ReactChildren | ReactChildren[];
}

const Breadcrumbs: React.FC<BradData> = ({ children }) => {
  return (
    <>
      <nav className="bg-grey-light p-3 rounded font-sans w-full m-4">
        <ol className="list-reset flex text-grey-dark">{children}</ol>
      </nav>
    </>
  );
};

export default Breadcrumbs;
