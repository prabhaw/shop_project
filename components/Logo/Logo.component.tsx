import React from "react";
import Image from "next/image";
interface DataProps {
  height: number;
  width: number;
  src: string;
  alt: string;
}
const Logo: React.FC<DataProps> = ({ height, width, src, alt }) => {
  return (
    <Image
      src={src}
      alt={alt}
      width={width}
      height={height}
      layout="intrinsic"
    />
  );
};

export default Logo;
