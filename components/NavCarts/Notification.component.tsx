import React from "react";
import Image from "next/image";
import { IoIosClose } from "@react-icons/all-files/io/IoIosClose";
import { IconContext } from "@react-icons/all-files";
import { Scrollbars } from "react-custom-scrollbars";
const shopdata = [
  {
    id: 1,
    image: "/images/laptop/l1.jpg",
    name: "motorola x95 mobile phone",
    price: 30000,
  },
  {
    id: 2,
    image: "/images/laptop/l1.jpg",
    name: "motorola x95 mobile phone",
    price: 20000,
  },
  {
    id: 3,
    image: "/images/laptop/l1.jpg",
    name: `Mamaearth Castor Oil For Healthier Skin, 
      Hair And Nails With 100% Pure And Natural Cold-Pressed Oil, 150Ml`,
    price: 1500,
  },
  {
    id: 4,
    image: "/images/laptop/l1.jpg",
    name: "And Nails With 100% Pure And Natural",
    price: 4000,
  },
  {
    id: 5,
    image: "/images/laptop/l1.jpg",
    name: "Hair And Nails With 100% Pure And Natural Cold-Pressed Oil, 150Ml",
    price: 3100,
  },
  {
    id: 6,
    image: "/images/laptop/l1.jpg",
    name: "Mamaearth Castor Oil For Healthier Skin,",
    price: 1200,
  },
];
const NotificationItems: React.FC = () => {
  return (
    <div className="rounded-md pb-2">
      <h5 className="font-semibold text-lg mx-[10px] my-2 text-secondary">
        Notification
      </h5>
      <hr />

      <div className="mx-[10px]  my-[2px]">
        <Scrollbars
          style={{
            width: "100%",
            height: "50vh",
          }}
          universal={true}
        >
          {shopdata.map((item, i) => (
            <div
              key={i}
              className=" flex space-x-2 py-2  cursor-pointer hover:bg-green-50 rounded-md"
            >
              <div className=" px-2 mt-1    ">
                <Image
                  src={item.image}
                  width={64}
                  height={64}
                  layout="intrinsic"
                  className="rounded-md"
                />
              </div>
              <div className="w-[300px]  mx-[8px]">
                <p className=" line-clamp-3  text-md text-gray-600 font-bold">
                  {item.name}
                </p>
              </div>
            </div>
          ))}
        </Scrollbars>
      </div>
    </div>
  );
};

export default NotificationItems;
