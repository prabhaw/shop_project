import React from "react";
import Image from "next/image";
import { IoIosClose } from "@react-icons/all-files/io/IoIosClose";
import { IconContext } from "@react-icons/all-files";
import { Scrollbars } from "react-custom-scrollbars";
const shopdata = [
  { id: 1, name: "motorola x95 mobile phone", price: 30000 },
  { id: 2, name: "motorola x95 mobile phone", price: 20000 },
  {
    id: 3,
    name: `Mamaearth Castor Oil For Healthier Skin, 
      Hair And Nails With 100% Pure And Natural Cold-Pressed Oil, 150Ml`,
    price: 1500,
  },
  { id: 4, name: "And Nails With 100% Pure And Natural", price: 4000 },
  {
    id: 5,
    name: "Hair And Nails With 100% Pure And Natural Cold-Pressed Oil, 150Ml",
    price: 3100,
  },
  { id: 6, name: "Mamaearth Castor Oil For Healthier Skin,", price: 1200 },
];
const ShoppingCartItem: React.FC = () => {
  return (
    <>
      <h5 className="font-semibold text-lg mx-[10px] my-2 text-secondary">
        Shopping Bag
      </h5>
      <hr />
      <div className="empty-text min-h-[200px]">
        <Image
          width={60}
          height={60}
          src="/empty-box.png"
          className="h-[60px] w-[60px]"
        />
        <h5 className="font-semibold  text-gray-400 text-lg">
          Shopping Bag is empty.
        </h5>
      </div>
      {/* <> <div className="mx-[10px]  my-[2px]">
       
        <Scrollbars
          style={{
            width: "100%",
            height: "50vh",
          }}
          universal={true}
        >
         {shopdata.map((item, i) => (
            <div key={item.id}>
              <div className="flex cursor-pointer hover:bg-gray-100 py-2  relative justify-start">
                <Image
                  width={70}
                  src={`/images/product.png`}
                  height={70}
                  className="object-cover rounded-lg h-[70px] w-70[px]"
                />
                <div className="w-[250px] mx-[8px]">
                  <p className=" line-clamp-1  text-base text-gray-500 font-semibold">
                    {item.name}
                  </p>
                  <p className={"text-xs text-gray-400 font-semibold"}>
                    2&nbsp;x&nbsp;300 = Rs.6000
                  </p>
                </div>
                <IconContext.Provider
                  value={{
                    className:
                      "text-red-500 text-2xl cursour-pointer absolute right-2 top-0 ",
                  }}
                >
                  <IoIosClose />
                </IconContext.Provider>
              </div>
              <hr className="border-b-[0.3px]   border-dashed border-gray-300 " />
            </div>
          ))} 
        </Scrollbars> 
      </div>
      <div className="flex py-2 mx-[10px] justify-between">
        <p className="text-base font-semibold text-gray-400">Total:</p>
        <p className="text-base font-semibold text-gray-400">Rs.500000</p>
      </div>
      <div className="mx-[10px] my-2 flex space-x-2 ">
        <button className="bg-primary hover:bg-yellow-400 font-semibold text-secondery focus:outline-none text-base w-full py-2">
          View Cart
        </button>
        <button className="bg-red-500 hover:bg-red-600 text-white focus:outline-none text-base font-semibold w-full py-2">
          Checkout
        </button>
      </div></> */}
      <style jsx>{`
        .empty-text {
          display: grid;
          place-items: center;
        }
      `}</style>
    </>
  );
};

export default ShoppingCartItem;
