import React, { ReactChildren, ReactChild, useEffect, useState } from "react";
import Image from "next/image";
import { IoIosClose } from "@react-icons/all-files/io/IoIosClose";
import { IconContext } from "@react-icons/all-files";
import { Scrollbars } from "react-custom-scrollbars";

interface PopProps {
  btnname: React.ReactNode | string;
  children?: ReactChild | ReactChild[] | ReactChildren | ReactChildren[];
}

const NavPopover: React.FC<PopProps> = ({ btnname, children }) => {
  return (
    <div className="group relative">
      <button className="focus:outline-none z-30 group-hover:text-primary relative inline-block">
        {btnname}
      </button>

      <div className=" transform scale-0 group-hover:scale-100 rounded-lg  transition duration-300 ease-in-out origin-top-right absolute  right-0 w-[400px] z-40   shadow-lg bg-white ring-1 ring-black ring-opacity-5  divide-gray-100 ">
        <div className="overflow-hidden bg-white rounded-lg shadow-lg mx-auto ">
          {children}
        </div>
      </div>
    </div>
  );
};

export default NavPopover;
