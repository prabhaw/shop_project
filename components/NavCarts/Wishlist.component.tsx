import React from "react";
import Image from "next/image";
import { IoIosClose } from "@react-icons/all-files/io/IoIosClose";
import { IconContext } from "@react-icons/all-files";
import { Scrollbars } from "react-custom-scrollbars";
const shopdata = [
  {
    id: 1,
    image: "/images/laptop/l1.jpg",
    name: "motorola x95 mobile phone",
    price: 30000,
  },
  {
    id: 2,
    image: "/images/laptop/l1.jpg",
    name: "motorola x95 mobile phone",
    price: 20000,
  },
  {
    id: 3,
    image: "/images/laptop/l1.jpg",
    name: `Mamaearth Castor Oil For Healthier Skin, 
      Hair And Nails With 100% Pure And Natural Cold-Pressed Oil, 150Ml`,
    price: 1500,
  },
  {
    id: 4,
    image: "/images/laptop/l1.jpg",
    name: "And Nails With 100% Pure And Natural",
    price: 4000,
  },
  {
    id: 5,
    image: "/images/laptop/l1.jpg",
    name: "Hair And Nails With 100% Pure And Natural Cold-Pressed Oil, 150Ml",
    price: 3100,
  },
  {
    id: 6,
    image: "/images/laptop/l1.jpg",
    name: "Mamaearth Castor Oil For Healthier Skin,",
    price: 1200,
  },
];
const WishListItem: React.FC = () => {
  return (
    <>
      <h5 className="font-semibold text-lg mx-[10px] my-2 text-secondary">
        Your Wishlist
      </h5>
      <hr />
      <div className="mx-[10px]  my-[2px]">
        <Scrollbars
          style={{
            width: "100%",
            height: "50vh",
          }}
          universal={true}
        >
          {shopdata.map((item, i) => (
            <div
              key={i}
              className="relative flex py-2 cursor-pointer hover:bg-green-100"
            >
              <div className=" px-2 mt-1    ">
                <Image
                  src={item.image}
                  width={64}
                  height={64}
                  layout="intrinsic"
                  className="rounded-md"
                />
              </div>
              <div className="w-[250px] mx-[8px]">
                <p className=" line-clamp-1  text-sm text-gray-500 font-semibold">
                  {item.name}
                </p>
                <div className="flex space-x-2">
                  <p className={"text-xs py-1 text-gray-400 font-semibold"}>
                    Rs.6000
                  </p>
                  <span className="bg-red-500 rounded-full py-1 px-2 text-xs font-semibold text-white">
                    2% discount
                  </span>
                </div>
              </div>
              <IconContext.Provider
                value={{
                  className:
                    "text-red-500 text-2xl cursour-pointer absolute right-2 top-0 ",
                }}
              >
                <IoIosClose />
              </IconContext.Provider>
            </div>
          ))}
        </Scrollbars>
      </div>
      <button className="bg-green-400 hover:bg-green-500 text-sm font-semibold text-white w-full py-3">
        View Full List
      </button>
    </>
  );
};

export default WishListItem;
