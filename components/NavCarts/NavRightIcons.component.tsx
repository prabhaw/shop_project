import React from "react";
import { IconContext } from "@react-icons/all-files";
import { BsHeart } from "@react-icons/all-files/bs/BsHeart";
import { BsBag } from "@react-icons/all-files/bs/BsBag";
import { IoMdNotificationsOutline } from "@react-icons/all-files/io/IoMdNotificationsOutline";
import NavPopover from "./NavPopover.component";
import ShoppingCartItem from "./ShoppingCartItems.component";
import WishListItem from "./Wishlist.component";
import NotificationItems from "./Notification.component";
interface navprops {
  lastnav?: boolean;
}
const NavRightIcons: React.FC<navprops> = ({ lastnav }) => {
  return (
    <div
      className={`flex relative mr-0 ${lastnav ? "mt-0  " : "mt-3"}  space-x-4`}
    >
      <NavPopover
        btnname={
          <div>
            <IconContext.Provider
              value={{
                className: `text-primary font-simibold                    text-[34px] 
                `,
              }}
            >
              <IoMdNotificationsOutline />
            </IconContext.Provider>
            <span className="top-0 right-0 absolute rounded-full w-2 h-2 bg-red-500  justify-center items-center"></span>
          </div>
        }
      >
        <NotificationItems />
      </NavPopover>

      <NavPopover
        btnname={
          <div className="">
            <IconContext.Provider
              value={{
                className: `text-primary font-simibold text-[28px]    `,
              }}
            >
              <BsHeart />
            </IconContext.Provider>
          </div>
        }
      >
        <WishListItem />
      </NavPopover>

      <NavPopover
        btnname={
          <div>
            <IconContext.Provider
              value={{
                className: `text-primary font-simibold text-[28px]  
                
                 `,
              }}
            >
              <BsBag />
            </IconContext.Provider>
          </div>
        }
      >
        <ShoppingCartItem />
      </NavPopover>
    </div>
  );
};

export default NavRightIcons;
