import React, { Fragment } from "react";
import { Popover, Transition } from "@headlessui/react";
import { FiChevronDown } from "@react-icons/all-files/fi/FiChevronDown";
import { BiMenuAltLeft } from "@react-icons/all-files/bi/BiMenuAltLeft";
import { IconContext } from "@react-icons/all-files";
import CategoryListItem from "@components/CategoryList/CategoryListItem.component";

const CategoryList: React.FC = () => {
  return (
    <Popover>
      {({ open }) => (
        <>
          <Popover.Button
            as="button"
            className={`relative text-primary bg-white px-2 h-[54px] w-72
               rounded-t-sm
              inline-flex items-center text-lg font-bold focus:outline-none`}
          >
            <IconContext.Provider
              value={{ className: "text-primary fontbold text-2xl mr-1" }}
            >
              <BiMenuAltLeft />
            </IconContext.Provider>
            <span>Product</span>
            <IconContext.Provider
              value={{
                className: `text-primary mt-1 font-bold text-2xl absolute right-3 transform  duration-300 ${
                  open ? "rotate-180" : ""
                }`,
              }}
            >
              <FiChevronDown />
            </IconContext.Provider>
          </Popover.Button>
          <Transition
            show={open ? true : false}
            as={Fragment}
            enter="transition linear duration-200"
            enterFrom="opacity-0  h-0"
            enterTo="opacity-100  h-[400px]"
            leave="transition linear duration-2000 "
            leaveFrom="opacity-100 translate-y-0 h-[400px]"
            leaveTo="opacity-0  h-0"
          >
            <Popover.Panel
              static
              className="absolute shadow-md z-10 w-56  flex "
            >
              <CategoryListItem />
            </Popover.Panel>
          </Transition>
        </>
      )}
    </Popover>
  );
};

export default CategoryList;
