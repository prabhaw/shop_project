import React from "react";
import { FiChevronRight } from "@react-icons/all-files/fi/FiChevronRight";

import { ImList2 } from "@react-icons/all-files/im/ImList2";
import { IconContext } from "@react-icons/all-files";
const solution = [
  { name: "Power Tools" },
  { name: "Electronic Devices" },
  { name: "Electronic Accessories" },
  { name: "TV & Home Appliances TV " },
  { name: "Health & Beauty" },
  { name: "Babies & Toys" },
  { name: "Watches & Accessories" },
  { name: "Sports & Outdoor" },
  { name: "Automotive & Motorbike" },
  { name: "Babies & Toys" },
];
const CategoryList: React.FC = () => {
  return (
    <>
      <div className=" min-w-full   ">
        <div className="relative grid w-72  pb-[4px]  bg-white ">
          {solution.map((item, i) => (
            <a
              className="px-2 py-3 cursor-pointer line-clamp-1 hover:bg-green-100 overflow-x-hidden text-primary font-bold text-md "
              key={i}
            >
              {item.name}
            </a>
          ))}

          <button className="px-2 relative flex py-3 cursor-pointer focus:outline-none  hover:bg-green-100  text-primary font-bold text-md ">
            <IconContext.Provider
              value={{
                className:
                  " font-base text-md mt-1 mr-1 text-primary font-bold text-md ",
              }}
            >
              <ImList2 />
            </IconContext.Provider>
            Product catalogs
            <IconContext.Provider
              value={{
                className: ` mt-1 font-semibold text-xl absolute right-3 text-primary font-bold text-md`,
              }}
            >
              <FiChevronRight />
            </IconContext.Provider>
          </button>
        </div>
      </div>
    </>
  );
};

export default CategoryList;
