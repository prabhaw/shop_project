import React from "react";
import { FiUser } from "@react-icons/all-files/fi/FiUser";
const navigation = [
  { name: "Product", href: "#" },
  { name: "Features", href: "#" },
  { name: "Marketplace", href: "#" },
  { name: "Company", href: "#" },
];

const Topbar: React.FC = () => {
  return (
    <>
      <div className="flex space-x-4 -mt-1  container overflow-x-auto mx-auto justify-center  lg:justify-between ">
        <div className="space-x-3 ">
          {navigation.map((item, i) => (
            <a
              key={i}
              href={item.href}
              className="text-xs font-semibold text-gray-500 hover:text-gray-900 whitespace-nowrap"
            >
              {item.name}
            </a>
          ))}
        </div>
        <div className="hidden  space-x-3 lg:flex">
          <span className="text-xs cursor-pointer font-semibold flex  text-gray-500 whitespace-nowrap hover:text-gray-900">
            <FiUser
              style={{ marginTop: "2px", marginRight: "2px" }}
              size="16px"
            />
            <span className="mt-1">prabhaw soti</span>
          </span>
          <span className="text-xs mt-1 cursor-pointer font-semibold flex  text-gray-500 whitespace-nowrap hover:text-gray-900">
            Log In
          </span>
          <span className="text-xs mt-1 cursor-pointer font-semibold flex  text-gray-500 whitespace-nowrap hover:text-gray-900">
            Sign Up
          </span>
        </div>
      </div>
    </>
  );
};

export default Topbar;
