import Logo from "@components/Logo/Logo.component";
import MainSearchInput from "@components/Search/MainpageSearch.component";
import React from "react";
import MidNavRightIcons from "@components/NavCarts/NavRightIcons.component";

const MiddleNav: React.FC = () => {
  return (
    <>
      <div className="flex  container  mx-auto   justify-between ">
        <div className="h-[40px] logo w-[220px]">
          <Logo width={220} height={40} src="/logo.png" alt="logo" />
        </div>
        <div style={{ width: "calc(100% - 600px)" }}>
          <MainSearchInput placeholder="Search For Product ......" />
        </div>
        <div className="">
          <MidNavRightIcons />
        </div>
      </div>
      <style jsx>{`
        .logo {
        }
      `}</style>
    </>
  );
};

export default MiddleNav;
