import CategoryList from "@components/CategoryList/CategoryList.component";
import React from "react";
import ShopNavItem from "@components/ShopNavItem/ShopNavItem.component";

const LastNav: React.FC = () => {
  return (
    <div className=" container flex   overflow-hidden  mx-auto justify-between ">
      <CategoryList />
      <div className="ml-8  flex mx-auto justify-between ">
        <ShopNavItem />
      </div>
    </div>
  );
};

export default LastNav;
