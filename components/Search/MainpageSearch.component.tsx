import React from "react";
import Select from "./searchSelect.component";
import SearchInput from "@components/Search/SearchInput.component";
interface InputProps {
  placeholder: string;
}
const MainSearchInput: React.FC<InputProps> = ({ placeholder }) => {
  return (
    <>
      <form
        // style={{ width: "calc(100% )" }s}
        method="GET"
        className="flex w-full    justify-center"
      >
        <div className="flex w-full rounded-sm ">
          <Select />
          <SearchInput placeholder={placeholder} />
        </div>
      </form>
    </>
  );
};

export default MainSearchInput;
