import React from "react";
import Image from "next/image";
const solution = [
  { name: "Power Tools" },
  { name: "Electronic Devices" },
  { name: "Electronic Accessories" },
  {
    name: "TV & Home Appliances TV & Home Appliances TV & Home Appliances TV & Home Appliances TV & Home Appliances TV & Home Appliances",
  },
  { name: "Health & Beauty" },
  { name: "Babies & Toys" },
  { name: "Watches & Accessories" },
  { name: "Sports & Outdoor" },
  { name: "Automotive & Motorbike" },
  { name: "Babies & Toys" },
];
const SearchOutput: React.FC = () => {
  return (
    <>
      {/* <div className="empty-text min-h-[100px]">
        <Image
          width={60}
          height={60}
          src="/empty-box.png"
          className="h-[60px] w-[60px]"
        />
        <h5 className="font-semibold  text-gray-600 text-base">
          No Data Were Found
        </h5>
      </div> */}

      <div>
        <h5 className="text-secondery font-semibold text-base pb-2">
          Your Recent Keywords
        </h5>
        <hr />
        {solution.map((item, i) => (
          <p
            key={i}
            className=" line-clamp-1 py-1 text-md hover:text-primary cursor-pointer"
          >
            {item.name}
          </p>
        ))}
      </div>
      {/* <div className="">
        {solution.map((item, i) => (
          <p
            key={i}
            className=" line-clamp-1 py-1 hover:text-primary cursor-pointer"
          >
            {item.name}
          </p>
        ))}
      </div> */}
      <style jsx>{`
        .empty-text {
          display: grid;
          place-items: center;
        }
      `}</style>
    </>
  );
};

export default SearchOutput;
