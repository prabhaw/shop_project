import React, { useState } from "react";
import SearchOutput from "./SearchOutput.component";

interface InputProps {
  placeholder: string;
}
const SearchInput: React.FC<InputProps> = ({ placeholder }) => {
  const [inputFocus, setFocus] = useState<boolean>(false);
  return (
    <>
      <div className="relative  w-full text-gray-400">
        <span className="absolute inset-y-0 py-4 right-0 flex items-center ">
          <button
            type="submit"
            className=" py-[15px]  px-6 text-white rounded-r-sm bg-gradient-to-r from-yellow-400 to-red-500  focus:outline-none "
          >
            <svg
              fill="none"
              stroke="currentColor"
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
              viewBox="0 0 24 24"
              className="w-6 h-6"
            >
              <path d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path>
            </svg>
          </button>
        </span>
        <input
          type="text"
          placeholder={placeholder}
          onFocus={() => {
            setFocus(true);
          }}
          onBlur={() => {
            setFocus(false);
          }}
          className="search-input w-full bg-second_color focus:outline-none py-4 pl-2 pr-10   text-md rounded-sm rounded-l-none"
        />
        {inputFocus && (
          <div className="absolute   z-30 bg-white shadow-lg py-4 px-4  w-full ">
            <SearchOutput />
          </div>
        )}
      </div>
      <style jsx>{`
        .search-input {
          /* width: calc(100%-500px) !impotant; */
        }
      `}</style>
    </>
  );
};

export default SearchInput;
