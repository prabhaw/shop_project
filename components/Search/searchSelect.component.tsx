import React from "react";

const Select: React.FC = (props) => {
  return (
    <>
      <select
        defaultValue=""
        className="search-input py-1 px-2  text-md bg-second_color outline-none  rounded-sm rounded-r-none text-gray-500 focus:outline-none"
      >
        <option disabled hidden value="">
          Choose Site
        </option>
        <option value="A">This Supplier</option>
        <option value="B">All Supplier</option>
      </select>
      <style jsx>{``}</style>
    </>
  );
};

export default Select;
