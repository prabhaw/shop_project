import React from "react";
import ReactStars from "react-rating-stars-component";
import { IconContext } from "@react-icons/all-files";
import { AiFillStar } from "@react-icons/all-files/ai/AiFillStar";
interface Rating {
  total: number;
}

const Reating: React.FC<Rating> = ({ total }) => {
  return (
    <>
      <div className="flex justify-center items-center">
        <div className="flex items-center mt-2 mb-4">
          {[...Array(total)].map((x, i) => (
            <IconContext.Provider
              key={i}
              value={{
                className: `text-primary 
        `,
              }}
            >
              <AiFillStar />
            </IconContext.Provider>
          ))}
          {[...Array(5 - total)].map((x, i) => (
            <IconContext.Provider
              key={i}
              value={{
                className: `text-gray-200 
        `,
              }}
            >
              <AiFillStar />
            </IconContext.Provider>
          ))}
        </div>
      </div>
    </>
  );
};

export default Reating;
