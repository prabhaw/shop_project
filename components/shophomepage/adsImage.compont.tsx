import React from "react";
import Image from "next/image";

const content = [
  { id: 1, image: "/images/ads/ads1.jpg" },
  { id: 2, image: "/images/ads/ads-2.jpg" },
  { id: 3, image: "/images/ads/ads-3.jpg" },
];
const AdsImage: React.FC = () => {
  return (
    <>
      <div className="flex w-full h-[400px] divide-y-[1px] divider-gray-50 divider-dash flex-col ">
        {content.map((item) => (
          <div key={item.id} className="">
            <Image
              src={item.image}
              height={3}
              width={5}
              alt=""
              layout="responsive"
            />
          </div>
        ))}
      </div>
    </>
  );
};

export default AdsImage;
