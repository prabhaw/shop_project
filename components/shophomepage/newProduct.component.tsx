import React from "react";
import Image from "next/image";
import Reating from "@components/Reating/reating.component";
import { FaArrowRight } from "@react-icons/all-files/fa/FaArrowRight";
import { IconContext } from "@react-icons/all-files";
import { AiFillHeart } from "@react-icons/all-files/ai/AiFillHeart";
const product = [
  {
    id: 1,
    name: "Samsung galaxy mobile n975",
    image: "/images/product/mobile-1.jpg",
    price: 40000,
  },
  {
    id: 2,
    name: "School Bag for student",
    image: "/images/product/bag.jpg",
    price: 40000,
  },

  {
    id: 3,
    name: "Samsung galaxy powerbake potable n975",
    image: "/images/product/pb-1.jpg",
    price: 40000,
  },
  {
    id: 3,
    name: "Samsung galaxy powerbake potable n975",
    image: "/images/product/pb-2.jpg",
    price: 40000,
  },
  {
    id: 4,
    name: "phont motorala x967 hadn set",
    image: "/images/product/phone-2.jpg",
    price: 40000,
  },
  {
    id: 5,
    name: "Samsung galaxy mobile n975",
    image: "/images/product/phone-3.jpg",
    price: 40000,
  },
  {
    id: 3,
    name: "Samsung galaxy powerbake potable n975",
    image: "/images/product/pb-2.jpg",
    price: 40000,
  },
];
interface productCardProps {
  item: {
    image: string;
    name: string;
    price: number;
  };
}
const ProductCard: React.FC<productCardProps> = ({ item }) => {
  return (
    <>
      <div className="flex p-2 bg-white justify-center relative">
        <button className="absolute top-4  focus:outline-none z-20 text-gray-300  hover:text-primary  right-4">
          <AiFillHeart size="28" />
        </button>
        <div className=" w-1/3  ">
          <div className=" my-2    mx-auto w-full ">
            <Image
              width={1}
              height={2}
              layout="responsive"
              src={item.image}
              // objectFit="cover"
              alt={item.name}
            />
          </div>
        </div>
        <div className="w-2/3 flex flex-wrap content-center   ">
          <div className="pl-4 pr-2 w-full">
            <p className="px-1  text-base text-gray-500 line-clamp-2 text-center">
              {item.name}
            </p>
            <div className="flex justify-center mx-auto">
              <p className="px-1 line-through py-1 font-semibold text-sm text-primary text-center">
                Rs.{item.price}
              </p>
              <p className="px-1  py-1 font-semibold text-sm text-primary text-center">
                Rs.{item.price}
              </p>
            </div>
            <div className="flex justify-center mx-auto space-x-2">
              <Reating total={3} />
              <span className="text-sm text-gray-500 mt-2">(20)</span>
            </div>
            <div className="flex justify-center ">
              <button className="bg-primary focus:outline-none hover:bg-green-500  rounded-sm text-white py-2 px-4">
                View Product
              </button>
            </div>
          </div>
        </div>
      </div>
      <style jsx>{`
        .set-center {
          display: grid;
          place-items: center;
        }
      `}</style>
    </>
  );
};

const HomeNewProduct: React.FC = () => {
  return (
    <>
      <div className="my-3">
        <h5 className="text-3xl mb-2 text-gray-500 font-semibold ">
          New Arrival Product
        </h5>
        <div className="grid  gap-2 lg:grid-cols-4 md:grid-cols-2 grid-cols-1">
          {product.map((item, i) => (
            <ProductCard key={i} item={item} />
          ))}

          <div className="bg-white set-center p-12 group cursor-pointer group">
            <div className=" transition duration-500 ease-in-out transform group-hover:-translate-y-1 group-hover:scale-110 rounded-full border-2 p-6 border-gray-400 group-hover:border-primary ">
              <IconContext.Provider
                value={{
                  className: "text-gray-400 text-6xl group-hover:text-primary ",
                }}
              >
                <FaArrowRight />
              </IconContext.Provider>
            </div>
          </div>
        </div>
      </div>
      <style jsx>{`
        .set-center {
          display: grid;
          place-items: center;
        }
      `}</style>
    </>
  );
};

export default HomeNewProduct;
