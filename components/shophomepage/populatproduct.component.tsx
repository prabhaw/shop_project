import React from "react";
import Image from "next/image";
import Carousel from "react-multi-carousel";
import { ButtonGroupProps } from "react-multi-carousel/lib/types";
import { GrFormNext } from "@react-icons/all-files/gr/GrFormNext";
import { GrFormPrevious } from "@react-icons/all-files/gr/GrFormPrevious";
import { IconContext } from "@react-icons/all-files";
import Reating from "@components/Reating/reating.component";
interface CarouselButtonGroupProps extends ButtonGroupProps {
  className?: string;
}

const product = [
  {
    id: 1,
    name: "Samsung galaxy mobile n975",
    image: "/images/product/mobile-1.jpg",
    price: 40000,
  },
  {
    id: 2,
    name: "School Bag for student",
    image: "/images/product/bag.jpg",
    price: 40000,
  },

  {
    id: 3,
    name: "Samsung galaxy powerbake potable n975",
    image: "/images/product/pb-1.jpg",
    price: 40000,
  },
  {
    id: 3,
    name: "Samsung galaxy powerbake potable n975",
    image: "/images/product/pb-2.jpg",
    price: 40000,
  },
  {
    id: 4,
    name: "phont motorala x967 hadn set",
    image: "/images/product/phone-2.jpg",
    price: 40000,
  },
  {
    id: 5,
    name: "Samsung galaxy mobile n975",
    image: "/images/product/phone-3.jpg",
    price: 40000,
  },
  {
    id: 3,
    name: "Samsung galaxy powerbake potable n975",
    image: "/images/product/pb-2.jpg",
    price: 40000,
  },
];

const ProductCard: React.FC<any> = ({ item }) => {
  return (
    <div className="cursor-pointer ">
      <div className="px-[2px] py-[1px]  transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-105 ">
        <div className="  min-x-[400px] px-4 py-4">
          <div className=" mb-2 h-[175px] w-[100px]  mx-auto w-full ">
            <Image
              width={4}
              height={7}
              layout="responsive"
              src={item.image}
              // objectFit="cover"
              alt={item.name}
            />
          </div>
          <p className="px-1  text-sm text-gray-500 line-clamp-2 text-center">
            {item.name}
          </p>
          <div className="flex justify-center mx-auto">
            <p className="px-1 line-through py-1 font-semibold text-sm text-primary text-center">
              Rs.{item.price}
            </p>
            <p className="px-1  py-1 font-semibold text-sm text-primary text-center">
              Rs.{item.price}
            </p>
          </div>
          <div className="flex justify-center mx-auto space-x-2">
            <Reating total={3} />
            <span className="text-sm text-gray-500 mt-2">(20)</span>
          </div>
        </div>
      </div>
    </div>
  );
};

const PopularProduct: React.FC = () => {
  const responsive = {
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 6,
    },
    tablet: {
      breakpoint: { max: 1024, min: 770 },
      items: 5,
    },
    largemobile: {
      breakpoint: { max: 770, min: 464 },
      items: 4,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 2,
    },
  };

  const ButtonGroup = ({
    next,
    previous,
    goToSlide,
    ...rest
  }: CarouselButtonGroupProps) => {
    const {
      carouselState: { currentSlide },
    } = rest;
    return (
      <div className="  ">
        <button
          className="left-0 top-28 absolute z-20 group  bg-green-200 hover:bg-green-300 focus:outline-none hover:text-white font-base  px-2 py-4"
          onClick={() => previous()}
        >
          <GrFormPrevious size={24} color="#ffff" />
        </button>
        <button
          onClick={() => next()}
          className=" top-28 right-0 absolute z-20 focus:outline-none bg-green-200 hover:bg-green-300 hover:text-white  font-base  px-2 py-4"
        >
          <GrFormNext size={24} color={"#ffff"} />
        </button>
      </div>
    );
  };

  return (
    <>
      <div className="relative  my-6">
        <h5 className="text-3xl mb-2 text-gray-500 font-semibold ">
          Popular Product
        </h5>

        <div className="shadow-lg bg-primary ">
          <Carousel
            swipeable={false}
            draggable={false}
            showDots={false}
            responsive={responsive}
            ssr={true} // means to render carousel on server-side.
            infinite={true}
            autoPlay={true}
            autoPlaySpeed={3000}
            keyBoardControl={true}
            //   customTransition="all .5"
            transitionDuration={500}
            customButtonGroup={<ButtonGroup />}
            arrows={false}
            containerClass="bg-white"
            //   centerMode={true}

            // removeArrowOnDeviceType={["tablet", "mobile"]}
            //   dotListClass="custom-dot-list-style"
            itemClass=" "
          >
            {product.map((item, i) => (
              <ProductCard key={item.id} item={item} />
            ))}
          </Carousel>
        </div>
      </div>
      <style jsx>{`
        /* .popular-word {
          width: 100%;
          text-align: left;
          border-bottom: 2px solid #b8b4b4;
          line-height: 0.1em;
          margin: 10px 0 20px;
        }
        .popular-word span {
          font-size: 1.875rem;
          margin-top: 5px;
        } */
      `}</style>
    </>
  );
};

export default PopularProduct;
