import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import SwiperCore, { Autoplay, Pagination } from "swiper/core";
import Image from "next/image";

const content = [
  { id: 1, image: "/images/slider/slide-3.jpg", name: "image1" },
  { id: 2, image: "/images/slider/slide-4.jpg", name: "image2" },
  { id: 3, image: "/images/slider/slide1.jpg", name: "image3" },
  { id: 4, image: "/images/slider/slide2.jpg", name: "image4" },
];

const SwiperSlider: React.FC = () => {
  SwiperCore.use([Pagination, Autoplay]);
  return (
    <>
      <Swiper
        slidesPerView={1}
        spaceBetween={0}
        loop={true}
        pagination={{
          clickable: true,
        }}
        autoplay={{
          delay: 5000,
          disableOnInteraction: false,
        }}
        className="mySwiper"
      >
        {content.map((item, i) => (
          <SwiperSlide key={item.id}>
            <Image src={item.image} alt={item.name} layout="fill" />
          </SwiperSlide>
        ))}
      </Swiper>

      <style jsx global>{`
        .swiper-container {
          width: 100%;
          height: 400px;
          // border-radius: 10px;
          z-index: 0 !important;
        }
        @media only screen and (max-width: 1079px) and (min-width: 1023px) {
          .swiper-container {
            width: 100%;
            height: 430px;
          }
          .swiper-slide img {
            display: block;
            width: 100%;
            height: 430px;
            object-fit: cover;
          }
        }

        @media only screen and (min-width: 1080px) and (min-width: 1279px) {
          .swiper-container {
            width: 100%;
            height: 448px;
          }
          .swiper-slide img {
            display: block;
            width: 100%;
            height: 448px;
            object-fit: cover;
          }
        }
        @media only screen and (max-width: 1280px) {
          .swiper-container {
            width: 100%;
            height: 430px;
          }
          .swiper-slide img {
            display: block;
            width: 100%;
            height: 430px;
            object-fit: cover;
          }
        }
        @media only screen and (max-width: 1023px) and (min-width: 700px) {
          .swiper-container {
            width: 100%;
            height: 300px;
          }

          .swiper-slide img {
            display: block;
            width: 100%;
            height: 300px;
            object-fit: cover;
          }
        }
        @media only screen and (max-width: 700px) {
          .swiper-container {
            width: 100%;
            height: 250px;
          }
          .swiper-slide img {
            display: block;
            width: 100%;
            height: 250px;
            object-fit: cover;
          }
        }
        @media only screen and (max-width: 400px) {
          .swiper-container {
            width: 100%;
            height: 200px;
          }
          .swiper-slide img {
            display: block;
            width: 100%;
            height: 200px;
            object-fit: cover;
          }
        }

        .swiper-slide {
          text-align: center;
          font-size: 18px;
          background: #fff;
          width: 100%;
          /* Center slide text vertically */
          display: -webkit-box;
          display: -ms-flexbox;
          display: -webkit-flex;
          display: flex;
          -webkit-box-pack: center;
          -ms-flex-pack: center;
          -webkit-justify-content: center;
          justify-content: center;
          -webkit-box-align: center;
          -ms-flex-align: center;
          -webkit-align-items: center;
          align-items: center;
        }
        // .swiper-slide img {
        //   display: block;
        //   width: 100%;
        //   height: 400px;
        //   object-fit: cover;
        // }

        .swiper-pagination-bullet {
          width: 12px;
          height: 6px;
          text-align: center;
          line-height: 20px;
          font-size: 12px;
          color: #eeee;
          opacity: 1;
          background: white;
          border-radius: 50%;
        }

        .swiper-pagination-bullet-active {
          background: #33a3b5;
        }
      `}</style>
    </>
  );
};

export default SwiperSlider;
