import React from "react";
import Image from "next/image";

interface ImageProps {
  height: number;
  width: number;
  image: string;
}

const InfoADS: React.FC<ImageProps> = ({ image, width, height }) => {
  return (
    <>
      <Image src={image} width={width} height={height} layout="responsive" />
    </>
  );
};

export default InfoADS;
