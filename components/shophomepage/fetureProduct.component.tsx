import React from "react";
import Reating from "@components/Reating/reating.component";
import Image from "next/image";
import { AiFillHeart } from "@react-icons/all-files/ai/AiFillHeart";
import { FaArrowRight } from "@react-icons/all-files/fa/FaArrowRight";
import { IconContext } from "@react-icons/all-files";
const content = [
  {
    id: 1,
    name: "HP ZBook Fury G8 (17.3-inch)",
    image: "/images/laptop/key1.jpg",
    price: 40000,
  },
  {
    id: 2,
    name: "HP ZBook Fury G8 (15.6-inch)",
    image: "/images/laptop/key2.jpg",
    price: 40000,
  },
  {
    id: 3,
    name: "Apple MacBook Air (M1 2020) Apple MacBook Air (M1 2020)Apple MacBook Air (M1 2020)",
    image: "/images/laptop/key3.jpg",
    price: 40000,
  },
  {
    id: 4,
    name: "Microsoft Surface Laptop 4",
    image: "/images/laptop/key-4.jpg",
    price: 40000,
  },
  {
    id: 5,
    name: "Dell XPS 15 (2020)",
    image: "/images/laptop/l1.jpg",
    price: 40000,
  },
  {
    id: 6,
    name: "HP Spectre x360 (2021)",
    image: "/images/laptop/l2.png",
    price: 40000,
  },
  {
    id: 7,
    name: "Dell XPS 13 (Late 2020)",
    image: "/images/laptop/l3.png",
    price: 40000,
  },
  {
    id: 8,
    name: "LG Gram 17 (2021)",
    image: "/images/laptop/l4.jpg",
    price: 40000,
  },
  { id: 9, name: "Acer Swift 3", image: "/images/laptop/l5.jpg", price: 40000 },
  {
    id: 10,
    name: " MacBook Pro (16-inch, 2019)",
    image: "/images/laptop/l6.jpg",
    price: 40000,
  },
  {
    id: 11,
    name: "Asus ROG Zephyrus G14",
    image: "/images/laptop/l7.jpg",
    price: 40000,
  },
  {
    id: 12,
    name: "Asus TUF Dash F15",
    image: "/images/laptop/l8.jpg",
    price: 40000,
  },
  {
    id: 13,
    name: "HP Elite Dragonfly",
    image: "/images/laptop/l9.jpg",
    price: 40000,
  },
  {
    id: 14,
    name: "HP Envy x360 13 (2020)",
    image: "/images/laptop/l10.jpg",
    price: 40000,
  },
  {
    id: 15,
    name: "Google Pixelbook Go",
    image: "/images/laptop/l11.png",
    price: 40000,
  },
  {
    id: 16,
    name: "Lenovo IdeaPad Duet Chromebook",
    image: "/images/laptop/l12.png",
    price: 40000,
  },
  {
    id: 17,
    name: "phont motorala x967 hadn set",
    image: "/images/product/phone-2.jpg",
    price: 40000,
  },
  {
    id: 18,
    name: "Samsung galaxy mobile n975",
    image: "/images/product/phone-3.jpg",
    price: 40000,
  },
  {
    id: 19,
    name: "Samsung galaxy powerbake potable n975",
    image: "/images/product/pb-2.jpg",
    price: 40000,
  },
];

const ProductCard: React.FC<any> = ({ item }) => {
  return (
    <>
      <div className="cursor-pointer  bg-white relative  ">
        <button className="absolute top-4  focus:outline-none z-20 text-gray-300  hover:text-primary  right-4">
          <AiFillHeart size="28" />
        </button>
        <div className="px-[2px] py-[1px]  transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-105 ">
          <div className="  min-x-[400px] px-4 py-4">
            <div className=" mb-2 h-full w-full  mx-auto  ">
              <Image
                width={2}
                height={2}
                layout="responsive"
                src={item.image}
                // objectFit="cover"
                alt={item.name}
              />
            </div>
            <p className="px-1  text-lg text-gray-500 line-clamp-2 text-center">
              {item.name}
            </p>
            <div className="flex justify-center mx-auto">
              <p className="px-1 line-through py-1 font-semibold text-sm text-primary text-center">
                Rs.{item.price}
              </p>
              <p className="px-1  py-1 font-semibold text-lg text-primary text-center">
                Rs.{item.price}
              </p>
            </div>
            <div className="flex justify-center mx-auto space-x-2">
              <Reating total={3} />
              <span className="text-sm text-gray-500 mt-2">(20)</span>
            </div>
          </div>
        </div>
      </div>

      <style jsx>{`
        .set-center {
          place-items: center;
        }
      `}</style>
    </>
  );
};

const FetureProduct: React.FC = () => {
  return (
    <>
      <div className="my-3">
        <h5 className="text-3xl mb-2 text-gray-500 font-semibold ">
          Feature Product
        </h5>
        <div className="grid  gap-2 lg:grid-cols-5 md:grid-cols-3 grid-cols-2">
          {content.map((item, i) => (
            <ProductCard key={item.id} item={item} />
          ))}

          <div className="flext justify-center bg-white set-center p-12 group cursor-pointer group">
            <div className=" transition duration-500 ease-in-out transform group-hover:-translate-y-1 group-hover:scale-110 rounded-full border-2 p-6 border-gray-400 group-hover:border-primary ">
              <IconContext.Provider
                value={{
                  className: "text-gray-400 text-6xl group-hover:text-primary ",
                }}
              >
                <FaArrowRight />
              </IconContext.Provider>
            </div>
          </div>
        </div>

        {/* <div className="flex justify-center py-8">
          <button className="focus:outline-none px-6 hover:bg-green-500 py-4 bg-primary text-white">
            LOAD MORE
          </button>
        </div> */}
      </div>
      <style jsx>{`
        .set-center {
          display: grid;
          place-items: center;
        }
      `}</style>
    </>
  );
};

export default FetureProduct;
