import { connect, useSelector } from "react-redux";
import { getItems } from "reduxs/fetchdata/fetchdata.action";
import { wrapper } from "reduxs/store";
import { RootState } from "@reduxs/index";
export default function Home(props: { data: [] }) {
  const data = useSelector((state: RootState) => state.data.foo);

  return (
    <div>
      {data.map((item) => (
        <div key={item.id}>
          <h6>{item.id}</h6>
          <h6>{item.title}</h6>
          <h6>{item.body}</h6>
          <br />
          <br />
          <br />
        </div>
      ))}
    </div>
  );
}

export const getServerSideProps = wrapper.getServerSideProps(
  async ({ store }) => {
    await store.dispatch(getItems());
  }
);
