import React from "react";
import Layout from "@layouts/supplier.layout";
import Gallery from "@components/GalleryPage/Gallery.component";

const OurGallery: React.FC = () => {
  return (
    <Layout>
      <div className=" px-4 lg:px-0 container ">
        <h2 className="py-4 font-bold text-lg text-gray-700">Our Gallery</h2>
        <Gallery />
      </div>
    </Layout>
  );
};

export default OurGallery;
