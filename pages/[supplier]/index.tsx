import React from "react";
import Layout from "@layouts/supplier.layout";
import SwiperSlider from "@components/shophomepage/SwiperSlider.component";
import PopularProduct from "@components/shophomepage/populatproduct.component";
import { getItems } from "reduxs/fetchdata/fetchdata.action";
import { wrapper } from "reduxs/store";
import AdsImage from "@components/shophomepage/adsImage.compont";
import HomeNewProduct from "@components/shophomepage/newProduct.component";
import FetureProduct from "@components/shophomepage/fetureProduct.component";
import InfoADS from "@components/shophomepage/info.component";

const SupplierHome: React.FC = () => {
  return (
    <Layout>
      <>
        <div className="grid  lg:space-x-[20px] xl:space-x-2 grid-cols-9 lg:grid-cols-12 xl:grid-cols-11 2xl:grid-cols-12 ">
          <div className="  lg:mt-1 xl:col-span-9 lg:col-span-9 col-span-9 2xl:col-span-10 ">
            <SwiperSlider />
          </div>
          <div
            className={`mt-1  h-12
           xl:col-span-2 hidden 
            lg:block 
            2xl:col-span-2 
            md:col-span-4 
            lg:col-span-3 h-[400px] `}
          >
            <AdsImage />
          </div>
        </div>
        <div className=" px-4 lg:px-0 container ">
          <div className="grid my-6  grid-flow-rows grid-cols-1 lg:grid-cols-3">
            <InfoADS height={1} width={3} image={"/images/pageads/ads1.jpg"} />
            <InfoADS height={1} width={3} image={"/images/pageads/ads2.jpg"} />
            <InfoADS height={1} width={3} image={"/images/pageads/ads3.jpg"} />
          </div>
          <PopularProduct />
          <div className="grid my-6  grid-flow-rows grid-cols-1 lg:grid-cols-3">
            <InfoADS height={1} width={3} image={"/images/pageads/ads1.jpg"} />
            <InfoADS height={1} width={3} image={"/images/pageads/ads2.jpg"} />
            <InfoADS height={1} width={3} image={"/images/pageads/ads3.jpg"} />
          </div>
          <HomeNewProduct />
          <div className="grid my-6  grid-flow-rows grid-cols-1 lg:grid-cols-3">
            <InfoADS height={1} width={3} image={"/images/pageads/ads1.jpg"} />
            <InfoADS height={1} width={3} image={"/images/pageads/ads2.jpg"} />
            <InfoADS height={1} width={3} image={"/images/pageads/ads3.jpg"} />
          </div>
          <FetureProduct />
        </div>
      </>
    </Layout>
  );
};
export const getServerSideProps = wrapper.getServerSideProps(
  async ({ store }) => {
    await store.dispatch(getItems());
  }
);
export default SupplierHome;
