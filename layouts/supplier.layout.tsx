import React, { ReactChildren, ReactChild, useEffect, useState } from "react";
import Topbar from "@components/Navigation/Topbar.component";
import MiddleNav from "@components/Navigation/MiddleNav.component";
import LastNav from "@components/Navigation/LastNav.component";

interface AuxProps {
  children: ReactChild | ReactChild[] | ReactChildren | ReactChildren[];
}
const SupplierLayout: React.FC<AuxProps> = ({ children }) => {
  const [scroll, setScroll] = useState<boolean>(false);

  useEffect(() => {
    const changeBackground = () => {
      if (window.scrollY >= 100) {
        setScroll(true);
      }
      if (window.scrollY <= 99) {
        setScroll(false);
      }
    };
    window.addEventListener("scroll", changeBackground);
    return () => {
      window.removeEventListener("scroll", changeBackground);
    };
  }, []);

  return (
    <div>
      <div className="lg:px-4 px-1 py-1 bg-white w-screen  ">
        <Topbar />
      </div>
      <div
        className={`${
          scroll ? "w-screen fixed  z-30 top-0" : "w-screen"
        }  lg:hidden block   shadow  `}
      ></div>
      <div className={`${scroll ? "fixed pt-[2px]  top-0" : ""} bg-white z-30`}>
        <div
          className={`
             h-[80px]
            px-4  bg-white w-screen  flex flex-wrap content-center `}
        >
          <MiddleNav />
        </div>
        <div
          className={` 
            
           px-4 h-[54px]  block bg-primary w-screen`}
        >
          <LastNav />
        </div>
      </div>
      <div className="  w-screen bg-bg_color  ">
        <div className="  container  mx-auto  ">{children}</div>
      </div>
    </div>
  );
};

export default SupplierLayout;
